<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |
## Diagram
![diagram](https://gitlab.com/hernand/aws_vpc_tf/-/raw/master/poc.svg?ref_type=heads)

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_subnet.private-1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.private-2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.public-1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.public-2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_availability_zones.az](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_availability_zone_names"></a> [availability\_zone\_names](#input\_availability\_zone\_names) | B0 \|\| list AZ | `list(string)` | <pre>[<br>  "eu-west-1"<br>]</pre> | no |
| <a name="input_docker_ports"></a> [docker\_ports](#input\_docker\_ports) | C0 \| Docker \| docker ports | <pre>list(object({<br>    internal = number<br>    external = number<br>    protocol = string<br>  }))</pre> | <pre>[<br>  {<br>    "external": 8300,<br>    "internal": 8300,<br>    "protocol": "tcp"<br>  }<br>]</pre> | no |
| <a name="input_enable_classiclink"></a> [enable\_classiclink](#input\_enable\_classiclink) | A1 \|\| Enable classiclink for vpc | `bool` | `false` | no |
| <a name="input_enable_dns_hostnames"></a> [enable\_dns\_hostnames](#input\_enable\_dns\_hostnames) | A1 \|\| Enable enable\_dns\_hostnames for vpc | `bool` | `false` | no |
| <a name="input_enable_dns_support"></a> [enable\_dns\_support](#input\_enable\_dns\_support) | A1 \|\| Enable dns support for vpc | `bool` | `false` | no |
| <a name="input_name"></a> [name](#input\_name) | A0 \|\| vpc name tagging | `string` | `"VPC01"` | no |
| <a name="input_password"></a> [password](#input\_password) | Z0 \| Credentials \| Password for poc | `string` | n/a | yes |
| <a name="input_private_1_ip"></a> [private\_1\_ip](#input\_private\_1\_ip) | A1 \|\| bool var ip priv subnet1 | `bool` | `false` | no |
| <a name="input_private_2_ip"></a> [private\_2\_ip](#input\_private\_2\_ip) | A1 \|\| bool var ip priv subnet2 | `bool` | `false` | no |
| <a name="input_public_1_ip"></a> [public\_1\_ip](#input\_public\_1\_ip) | A1 \|\| bool var ip subnet1 | `bool` | `false` | no |
| <a name="input_public_2_ip"></a> [public\_2\_ip](#input\_public\_2\_ip) | A1 \|\| bool var ip subnet2 | `bool` | `false` | no |
| <a name="input_region"></a> [region](#input\_region) | A2 \|\| AWS region set | `string` | `"eu-west-1"` | no |
| <a name="input_subnet_cidr"></a> [subnet\_cidr](#input\_subnet\_cidr) | A0 \| Network \| map of string subnet\_cidr | `map(string)` | <pre>{<br>  "be1": "10.0.0.0/24",<br>  "be2": "10.0.1.0/24",<br>  "fe1": "10.0.2.0/24",<br>  "fe2": "10.0.3.0/24"<br>}</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | A1 \|\| default vpc\_cidr | `string` | `"10.0.0.0/16"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cidr_ip"></a> [cidr\_ip](#output\_cidr\_ip) | n/a |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | n/a |
<!-- END_TF_DOCS -->